local awful = require( 'awful' )
local gears = require( 'gears' )
local wibox = require( 'wibox' )

Terminal = "st"
Editor = os.getenv("EDITOR") or "nvim"
Editor_cmd = Terminal .. " -e " .. Editor

-- Keyboard map indicator and switcher
Mykeyboardlayout = awful.widget.keyboardlayout()

-- Create a textclock widget
Mytextclock = wibox.widget.textclock()

Modkey = 'Mod1'

-- Create a wibox for each screen and add it
Taglist_buttons = gears.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ Modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ Modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
                )

Tasklist_buttons = gears.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  c:emit_signal(
                                                      "request::activate",
                                                      "tasklist",
                                                      {raise = true}
                                                  )
                                              end
                                          end),
                     awful.button({ }, 3, function()
                                              awful.menu.client_list({ theme = { width = 250 } })
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                          end))

KeyboardWidget = wibox.widget {
	image = '/home/topcat/pic/test.svg',
	resize = true,
	widget = wibox.widget.imagebox,
}

function Ram()
	return io.popen("free -h | awk 'NR==2 {print $3\"/\"$2}' | sed 's/i//g'" ):read( '*all' ):gsub( '\n', '' )
end

function Temp()
	return io.popen( "sensors | grep temp1 | awk \'NR==2\' | sed \'s/(crit = +120.0°C)//g\' | sed \'s/temp1:        +//g\' | sed \'s/ //g\'" ):read( '*all' ):gsub( '\n', '' )
end

function Bat()
	return io.popen( 'cat /sys/class/power_supply/BAT0/capacity' ):read( '*all' ):gsub( '\n', '' )
end

local command = 'bash -c "'.. 'free -h | awk \'' .. 'NR==2 {print ' .. '$3\"' .. '//' .. '\"$2} | sed \'s/i//g\''


RamWidget = wibox.widget({
	{
		{
			{
				image = '/home/topcat/pic/ramIcon.svg',
				resize = true,
				widget = wibox.widget.imagebox
			},
			{
				widget = awful.widget.watch( 'bash -c ramInfo', 1 ),
				font = 'Hasklug Bold 10'

			},
			layout = wibox.layout.align.horizontal,
		},
		left = 5,
		right = 5,
		widget = wibox.container.margin
	},

	widget = wibox.container.background,
	bg = '#98971a',
	shape = function( c, w, h ) return gears.shape.rounded_rect( c, w, h, 5 ) end

})

CpuWidget = wibox.widget({
	{
		{
			{
				image = '/usr/share/icons/Papirus-Dark/64x64@2x/apps/cpu-x.svg',
				resize = true,
				widget = wibox.widget.imagebox
			},
--			spacing = 7,
--			{
--				text = Cpu(),
--				widget = wibox.widget.textbox,
--			},
			{
				widget = awful.widget.watch( 'bash -c "cat /proc/loadavg | awk \'{print $1}\'"', 1 ),
				font = 'Hasklug Bold 10'

			},
			layout = wibox.layout.align.horizontal,
			bg = '#b16286',
		},
		left = 5,
		right = 5,
		widget = wibox.container.margin
	},
	widget = wibox.container.background,
	bg = '#b16286',
	shape = function( c, w, h ) return gears.shape.rounded_rect( c, w, h, 5 ) end
})

TempWidget = wibox.widget({
	{
		{
			{
				image = '/usr/share/icons/Papirus-Dark/64x64@2x/apps/psensor.svg',
				resize = true,
				widget = wibox.widget.imagebox
			},
			spacing = 7,
			{
				widget = awful.widget.watch( 'bash -c "sensors | grep temp1 | awk \'NR==2\' | sed \'s/(crit = +120.0°C)//g\' | sed \'s/temp1:        +//g\' | sed \'s/ //g\'"', 1 ),
				font = 'Hasklug Bold 10'
			},
			layout = wibox.layout.align.horizontal,
			bg = '#b16286',
		},
		left = 5,
		right = 5,
		widget = wibox.container.margin
	},
	widget = wibox.container.background,
	bg = '#689d6a',
	shape = function( c, w, h ) return gears.shape.rounded_rect( c, w, h, 5 ) end
})

BatteryWidget = wibox.widget({
	{
		{
			{
				image = '/home/topcat/pic/batteryIcon.svg',
				resize = true,
				widget = wibox.widget.imagebox,
			},
			spacing = 7,
			{
				widget = awful.widget.watch( 'bash -c "cat /sys/class/power_supply/BAT0/capacity "', 1 ),
				font = 'Hasklug Bold 10'
			},
			layout = wibox.layout.align.horizontal,
			bg = '#d79921',
		},
		left = 7,
		right = 7,
		widget = wibox.container.margin
	},
	widget = wibox.container.background,
	bg = '#d79921',
	shape = function( c, w, h ) return gears.shape.rounded_rect( c, w, h, 5 ) end
})


function KeyLayout()
	local option = io.popen( "setxkbmap -query | grep lay | awk '{print $2}'" )
	if option then option = option:read('*all'):gsub( "\n", "" ) end

	if option == 'us' then
		os.execute( 'setxkbmap br' )
		KeyboardWidget.image = '/home/topcat/pic/brazilFlagRound.svg'
	else
		os.execute( 'setxkbmap us' )
		KeyboardWidget.image = '/home/topcat/pic/test.svg'
	end
end

--psensor.svg
--cpu-x
