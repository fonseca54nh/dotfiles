local awful = require( 'awful' )
local beautiful = require( 'beautiful' )
local gears = require( 'gears' )

awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     screen = awful.screen.preferred,
                     --placement = awful.placement.no_overlap+awful.placement.no_offscreen
		     placement = awful.placement.centered
     }
    },
    {
	    rule= { class="Gimp", class='Inkscape' },
	    properties =
	    {
		switchtotag=true,
		screen = 1,
		tag = screen[1].tags[ 5 ]
	    },
    },
    {
	    rule= { class= "Brave-browser" },
	    properties =
	    {
		switchtotag=true,
		screen = function() if screen[2] then return 2 else return 1 end end,
		tag = function() if screen[2] then return screen[2].tags[1] else return screen[1].tags[2] end end
	    },
    },
    {
	    rule= { class= "Ardour*" },
	    properties =
	    {
		switchtotag=true,
		screen = 1,
		tag = screen[1].tags[7]
	    },
    },
    {
	    rule= { class= "Zathura" },
	    properties =
	    {
		switchtotag=true,
		tag = screen[1].tags[3]
	    },
    },
    {
	    rule= { class= "Pavucontrol" },
	    properties =
	    {
		switchtotag=true,
		tag = screen[1].tags[8]
	    },
    },
    -- Floating clients.
    { rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
          "pinentry",
        },
        class = {
          "Arandr",
          "Blueman-manager",
          "Gpick",
          "Kruler",
          "MessageWin",  -- kalarm.
          "Sxiv",
          "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
          "Wpa_gui",
          "veromix",
          "xtightvncviewer"},

        name = {
          "Event Tester",  -- xev.
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "ConfigManager",  -- Thunderbird's about:config.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
      }, properties = { floating = true }},

    -- Add titlebars to normal clients and dialogs
    { rule_any = {type = { "normal", "dialog" }
      }, properties = { titlebars_enabled = false }
    },

}

client.connect_signal("manage", function (c)
    if awesome.startup
      and not c.size_hints.user_position
      and not c.size_hints.program_position then
        awful.placement.no_offscreen(c)
    end
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    c:emit_signal("request::activate", "mouse_enter", {raise = false})
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)

-- rounded corners
client.connect_signal( "manage", function( c )
	c.shape = gears.shape.rounded_rect
end)
