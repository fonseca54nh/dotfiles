-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
local beautiful = require("beautiful")
local naughty = require( 'naughty' )

require( 'errors' )
require( 'bar' )
require( 'keys' )
require( 'rules' )
--require( 'nots' )

--beautiful.init(gears.filesystem.get_themes_dir() .. "default/theme.lua")
beautiful.init( "/home/topcat/.config/awesome/default/theme.lua" )

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.spiral,
    awful.layout.suit.spiral.dwindle,
    awful.layout.suit.max,
    awful.layout.suit.max.fullscreen,
    awful.layout.suit.magnifier,
    awful.layout.suit.corner.nw,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
}

--root.buttons(gears.table.join(
    --awful.button({ }, 3, function () mymainmenu:toggle() end)
    --awful.button({ }, 4, awful.tag.viewnext),
    --awful.button({ }, 5, awful.tag.viewprev)
--))


-- really simple notification
--naughty.notify( { shape=gears.shape.rounded_rect, icon='/home/topcat/pic/tripleFork.png', text='hello there' } )
awful.spawn( 'feh --bg-scale /home/topcat/pic/wallpaper/gruvbox/gruvbox.jpg' )
