local awful = require( 'awful' )
local gears = require( 'gears' )

Modkey = 'Mod1'

Globalkeys = gears.table.join(
    awful.key({ Modkey , } , "s"      , hotkeys_popup.show_help                      , {description = "show help"               , group = "awesome"}) ,
    awful.key({ Modkey , } , "Left"   , awful.tag.viewprev                           , {description = "view previous"           , group = "tag"})     ,
    awful.key({ Modkey , } , "Right"  , awful.tag.viewnext                           , {description = "view next"               , group = "tag"})     ,
    awful.key({ Modkey , } , "Escape" , awful.tag.history.restore                    , {description = "go back"                 , group = "tag"})     ,

    awful.key({ Modkey , } , "j"      , function () awful.client.focus.byidx( 1) end , {description = "focus next by index"     , group = "client"})  ,
    awful.key({ Modkey , } , "k"      , function () awful.client.focus.byidx(-1) end , {description = "focus previous by index" , group = "client"})  ,
    awful.key({ Modkey , } , "w"      , function () mymainmenu:show() end            , {description = "show main menu"          , group = "awesome"}) ,
    --
    -- Layout manipulation
    awful.key({ Modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end, {description = "swap with next client by index", group = "client"}),
    awful.key({ Modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end, {description = "swap with previous client by index", group = "client"}),
    awful.key({ Modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end, {description = "focus the next screen", group = "screen"}),
    awful.key({ Modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end, {description = "focus the previous screen", group = "screen"}),
    awful.key({ Modkey,           }, "u", awful.client.urgent.jumpto, {description = "jump to urgent client", group = "client"}),
    awful.key({ Modkey,           }, "Tab", function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
        {description = "go back", group = "client"}),

    -- Standard program
    awful.key({ Modkey,           }, "Return", function () awful.spawn(Terminal) end,
              {description = "open a Terminal", group = "launcher"}),
    awful.key({ Modkey, "Control" }, "r", awesome.restart,
              {description = "reload awesome", group = "awesome"}),
    awful.key({ Modkey, "Shift"   }, "q", awesome.quit,
              {description = "quit awesome", group = "awesome"}),

    awful.key({ Modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)          end,
              {description = "increase master width factor", group = "layout"}),
    awful.key({ Modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)          end,
              {description = "decrease master width factor", group = "layout"}),
    awful.key({ Modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1, nil, true) end,
              {description = "increase the number of master clients", group = "layout"}),
    awful.key({ Modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1, nil, true) end,
              {description = "decrease the number of master clients", group = "layout"}),
    awful.key({ Modkey, "Control" }, "h",     function () awful.tag.incncol( 1, nil, true)    end,
              {description = "increase the number of columns", group = "layout"}),
    awful.key({ Modkey, "Control" }, "l",     function () awful.tag.incncol(-1, nil, true)    end,
              {description = "decrease the number of columns", group = "layout"}),
    --awful.key({ Modkey,           }, "space", function () awful.layout.inc( 1)                end,
    awful.key({ Modkey,           }, "space", function () awful.spawn( 'dmenu_run' )end,
              {description = "select next", group = "layout"}),
    awful.key({ Modkey, "Shift"   }, "space", function () awful.layout.inc(-1)                end,
              {description = "select previous", group = "layout"}),

    awful.key({ Modkey, "Control" }, "n",
              function ()
                  local c = awful.client.restore()
                  -- Focus restored client
                  if c then
                    c:emit_signal(
                        "request::activate", "key.unminimize", {raise = true}
                    )
                  end
              end,
              {description = "restore minimized", group = "client"}),

    -- Prompt
    --awful.key({ Modkey },            "r",     function () awful.screen.focused().mypromptbox:run() end,
    awful.key({ Modkey, "Shift" },            "b",    function() awful.spawn( 'lua /home/topcat/scripts/mymenu/mymenu.lua' )end,
              {description = "run prompt", group = "launcher"}),

    awful.key({ Modkey }, "x",
              function ()
                  awful.prompt.run {
                    prompt       = "Run Lua code: ",
                    textbox      = awful.screen.focused().mypromptbox.widget,
                    exe_callback = awful.util.eval,
                    history_path = awful.util.get_cache_dir() .. "/history_eval"
                  }
              end,
              {description = "lua execute prompt", group = "awesome"}),
    -- Menubar
    awful.key({ Modkey }, "p", function() menubar.show() end,
              {description = "show the menubar", group = "launcher"})
)

Clientkeys = gears.table.join(
    awful.key({ Modkey,           }, "f", function (c) c.fullscreen = not c.fullscreen c:raise() end, {description = "toggle fullscreen", group = "client"}),
    awful.key({ Modkey, "Shift"   }, "c",      function (c) c:kill()                         end, {description = "close", group = "client"}),
    awful.key({ Modkey, "Control" }, "space",  awful.client.floating.toggle                     , {description = "toggle floating", group = "client"}),
    awful.key({ Modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end, {description = "move to master", group = "client"}),
    awful.key({ Modkey,           }, "o",      function (c) c:move_to_screen()               end, {description = "move to screen", group = "client"}),
    awful.key({ Modkey,           }, "t",      function (c) c.ontop = not c.ontop            end, {description = "toggle keep on top", group = "client"}),
    awful.key({ Modkey,           }, "n", function (c) c.minimized = true end , {description = "minimize", group = "client"}),
    awful.key({ Modkey,           }, "m", function (c) c.maximized = not c.maximized c:raise() end , {description = "(un)maximize", group = "client"}),
    awful.key({ Modkey, "Control" }, "m", function (c) c.maximized_vertical = not c.maximized_vertical c:raise() end , {description = "(un)maximize vertically", group = "client"}),
    awful.key({ Modkey, "Shift"   }, "m", function (c) c.maximized_horizontal = not c.maximized_horizontal c:raise() end , {description = "(un)maximize horizontally", group = "client"})
)
