local awful = require( 'awful' )
local gears = require( 'gears' )
local wibox = require( 'wibox' )
local beautiful = require( 'beautiful' )
local dpi = require("beautiful").xresources.apply_dpi

require( 'apps' )

local function set_wallpaper(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
	for s = 1, screen.count() do
		gears.wallpaper.maximized(wallpaper, s, true)
	end
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

local ip = '/usr/share/icons/Papirus-Dark/64x64@2x/apps/'
local tags = {
	names = { "1", "2", "3", "4", "5", "6", "7", "8", "9" },
	layout = { awful.layout.layouts[1] },
	icons = {
		ip .. 'gvim.svg',
		ip .. 'brave-browser.svg',
		ip .. 'org.homelinuxserver.vance.biblereader-symbolic.svg',
		ip .. 'steam-icon.svg',
		ip .. 'krita.svg',
		ip .. 'apple-music.svg',
		ip .. 'ardour6.svg',
		ip .. 'pavucontrol.svg',
		ip .. 'tux.svg',
	},
	colors = { '#458588' },
	bg = '#b16286',
}

awful.screen.connect_for_each_screen(function(s)
	-- Wallpaper
	set_wallpaper( s )

	-- Each screen has its own tag table.
	tags[s] = awful.tag( tags.names, s, tags.layout )
	for i, t in ipairs( tags[s] ) do
		awful.tag.seticon( tags.icons[i], t )
		awful.tag.setproperty( t, "icon_only", 1 )
	end
	-- Create a promptbox for each screen
	s.mypromptbox = awful.widget.prompt()

	-- Create an imagebox widget which will contain an icon indicating which layout we're using.
	-- We need one layoutbox per screen.
	s.mylayoutbox = awful.widget.layoutbox(s)
	s.mylayoutbox:buttons(gears.table.join(
			   awful.button({ }, 1, function () awful.layout.inc( 1) end),
			   awful.button({ }, 3, function () awful.layout.inc(-1) end),
			   awful.button({ }, 4, function () awful.layout.inc( 1) end),
			   awful.button({ }, 5, function () awful.layout.inc(-1) end)))

	-- Create a taglist widget
	s.mytaglist = awful.widget.taglist {
		screen  = s,
		filter  = awful.widget.taglist.filter.all,
		buttons = Taglist_buttons
	}

	-- Create a tasklist widget
	s.mytasklist = awful.widget.tasklist {
		screen  = s,
		filter  = awful.widget.tasklist.filter.currenttags,
		buttons = Tasklist_buttons
	}

	-- Create the top left bar
	s.mywibox = awful.wibar(
	{
		widget = wibox.container.background,
		position = "top",
		screen = s,
		visible = true,
		width = dpi( 725 ),
		height = dpi( 25 ),
		placement = function( c ) awful.placement.top_right( c, { margins = dpi( 10 ) } ) end,
		border_color = '#ffffff',
		border_width = 2,
		shape = function( cr, w, h )
		    gears.shape.rounded_rect( cr, w, h, 5 )
		end
	})

	-- change bar position
--	s.mywibox.x = 1500
	s.mywibox.y = 10

	-- Add widgets to the wibox
	s.mywibox:setup
	{
		layout = wibox.layout.fixed.horizontal,
		{ -- Left widgets
		    layout = wibox.layout.fixed.horizontal,
		    --mylauncher,
		    s.mytaglist,
		    s.mypromptbox,
		},
		--s.mytasklist, -- Middle widget
		    spacing = 7,
		{ -- Right widgets
		    KeyboardWidget,
		    layout = wibox.layout.fixed.horizontal,
		    RamWidget,
		    spacing = 7,
		    CpuWidget,
		    TempWidget,
		    BatteryWidget,
		    --Mykeyboardlayout,
		    wibox.widget.systray(),
		    Mytextclock,
		    --s.mylayoutbox,
		},
	}
end)
