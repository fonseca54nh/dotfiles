-- bunch of sets
return [[
	set fsync
	set mouse=nicr
	set number
	set rnu
	set autoindent
	set t_Co=256
	set mousehide
	set clipboard=unnamedplus
	set wildmode=longest,list,full
	set nocursorline
	set nocursorcolumn
	set lazyredraw
	"set nrformats+=alpha
	syntax enable
	syntax on
	filetype plugin indent on
	"set cole=2
	set laststatus=2
	set noshowmode
	set foldmethod=expr
	let mapleader=";"

]]
