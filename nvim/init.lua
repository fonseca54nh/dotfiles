ConfigTable  = function( path ) return vim.fn.glob( '~/.config/nvim/' .. path , false, true ) end
ConfigFolder = vim.fn.glob( '~/.config/nvim/' )

-- set all the options
vim.cmd( dofile( ConfigFolder .. 'opts.lua' ) )

-- load all the keybindings
local keybindings = dofile( ConfigFolder .. 'keybindings.lua' )

--set all the keymaps defined at the keybinding table
for _,i in ipairs( keybindings ) do
	vim.keymap.set( i[1], i[2], i[3] )
end

-- save file view when closing the file
vim.api.nvim_create_autocmd( 'BufWinLeave', {
	pattern = {'*.*'},
	command = 'mkview'
} )

-- load file view when opening the file
vim.api.nvim_create_autocmd( 'BufWinEnter', {
	pattern = {'*.*'},
	command = 'silent! loadview'
} )

-- init all the plugins
dofile( ConfigFolder .. './lazy.lua') 

-- set the colorscheme after all plugins are loaded
vim.cmd( 'set termguicolors' )
vim.cmd( 'colorscheme caret' )
