-- table containing all the keybindings
return {
	{ 'n', '<space>w', ':w<cr>'	},
	{ 'n', '<space>q', ':q!<cr>'	},
	{ 'n', '<space>a', ':qa!<cr>'	},
	{ 'n', '<space>e', ':e'		},
	{ 'n', '<leader>1', '0i!<esc>'	},
	{ 'n', '<leader>1', ':norm ^i!<cr>'},

	{ 'n', "<leader>'", '0i"<esc>'     } ,
	{ 'n', "<leader>'", ':norm i"<cr>' } ,

	{ 'n', '<leader>3', '0i#<esc>'     } ,
	{ 'n', '<leader>3', ':norm i#<cr>' } ,

	{ 'n', '<leader>-', '0i--<esc>'     } ,
	{ 'n', '<leader>-', ':norm i--<cr>' } ,

	{ 'n', '<leader>6', ':norm ^x<cr>'},

	{ 'n', '<leader>h', 'i<!---<esc>}<esc>kA!---><esc>'},

	{ 'n', '<leader><leader>v', ':vs<cr>'},
	{ 'n', '<leader><leader>s', ':sp<cr>'},

	{ 'n', '<c-j>', '<c-w><c-j>'},
	{ 'n', '<c-k>', '<c-w><c-k>'},
	{ 'n', '<c-h>', '<c-w><c-h>'},
	{ 'n', '<c-l>', '<c-w><c-l>'},

	{ 't', '<esc>', '<C-\\><C-n>'},

	{'n', '<space>n', ':tabnew<cr>'       } ,
	{'n', '<space>j', ':tabprev<cr>'      } ,
	{'n', '<space>j', '<esc>:tabprev<cr>' } ,
	{'n', '<space>k', ':tabnext<cr>'      } ,
	{'n', '<space>k', '<esc>:tabnext<cr>' } ,

	{'n', '<space>tt', ':term<cr>'},

	{'i', '<leader><leader>', '<esc>/<++><enter>"_c4l' } ,
	{'i', '<c-G>', '<esc>G'                            } ,
	{'i', '<leader>a', '<esc>A'                        } ,
	{'i', '<leader>l', '<esc>lli'                      } ,

	{'n', '<leader><leader>ee', 'Vj%zf'},

	{'n', '<leader>rv', ':source $MYVIMRC<cr>'   } ,
	--{'n', '<leader>ev', ':tabnew $MYVIMRC<cr>' } ,
	{'n', '<leader>ev', ':tabnew ~/vimrc.md<cr>' } ,
	{'n', '<space>e', ':e' .. ' '                } ,

	{'n', '<space>ofuc', ':tabe **/<cfile><cr>'},

	--{'n', '<space>mt', ':lua compileFile()' } ,
	{'n', '<space>m', ':make<cr>'             } ,
	{'n', '<space>b', ':!./bin/*<cr>'         } ,

	{'n', '<leader>p', ':!zathura %:r.pdf&<CR><CR>'},

	{'n', '<leader>tel', ':Telescope '               } ,
	{'n', '<leader>ff',  ':Telescope find_files<cr>' } ,
	{'n', '<leader>cd',  ':Telescope cder<cr>'       } ,

	{'n', '<leader>ng', ':Neogit<cr>'},

	--{'n', '<leader>rv', ':source /root/.config/nvim/init.lua<enter>'},

	{'n', '<leader>o', ':NERDTreeToggle<cr>'},

	{'n', '<leader>ka', ':KanbanOpen ~/vimwiki/tasks.md<cr>' } ,
	{'n', '<leader>ko', ':KanbanOpen '                       } ,
	{'n', '<leader>kc', ':KanbanCreate '                     } ,

	{'n', '<F3>', ':lua require("runner").run()<cr>' },
	{'n', '<F5>', ':lua require("runner").autorun()<cr>' },
	{'n', '<F6>', ':lua require("runner").autorun_stop()<cr>' },

	{'n', '<leader><space>b', ':lua require("dap").toggle_breakpoint()<cr>'},
	{'n', '<leader><space>s', ':lua require("dap").step_into()<cr>'},
	{'n', '<leader><space>c', ':lua require("dap").continue()<cr>'},
	{'n', '<leader><space>r', ':lua require("dap").repl.open()<cr>'},

	{'n', '<F3>', ':OverseerOpen<cr>' },
	{'n', '<F4>', function() 
		vim.cmd('OverseerOpen')
		vim.cmd('OverseerRun')
	end},

	{'n', '`', ':Telescope cmdline<cr>'},

	--{{'i', 's'}, '<Tab>', function ()
	--	if luasnip.expand_or_jumpable() then
	--		luasnip.expand_or_jump()
	--	else
	--		vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes("<Tab>", true, false, true), "n", false)
	--	end
	--end, silent=true},
}
