local ls = require( 'luasnip' )
require("luasnip.loaders.from_vscode").lazy_load()

vim.keymap.set({'i'}, '<Tab>', function () ls.expand() end, {silent=true})
