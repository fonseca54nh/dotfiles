local neogit = require('neogit')
neogit.setup {
	preview_buffer = { kind = 'split' },
	telescope_sorter = function() return require("telescope").extensions.fzf.native_fzf_sorter() end,
	integrations = { telescope = true },
}
