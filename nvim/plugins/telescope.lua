local telescopeConfig = require("telescope.config")
--local vimgrep_arguments = { unpack(telescopeConfig.values.vimgrep_arguments) }
--table.insert(vimgrep_arguments, "--hidden")

local project_actions = require("telescope._extensions.project.actions"),
--require('telescope').load_extension('telescope-code-fence')
require('telescope').load_extension('cmdline')
require('telescope').load_extension('docker')
require('telescope').load_extension('project')
require('telescope').load_extension('file_browser')
require('telescope').load_extension('cder')
require('telescope').setup({
	pickers = {
		colorscheme = {
			enable_preview = true
		},

		find_files = {
			--find_command = { 'fd', '--hidden', '--type=d', '.', os.getenv('HOME') },
			--find_command = { 'rg', '--files', '--hidden', '--glob', '!**/.git/*'},
			--dir_command = { 'echo', '"piranha"'},
			hidden=true,

			mappings = {
				n = {
					['cd'] = function( prompt_bufnr )
						local selection = require( 'telescope.actions.state').get_selected_entry()
						local dir = vim.fn.fnamemodify(selection.path, ':p:h')
						require('telescope.actions').close(prompt_bufnr)
						vim.cmd(string.format('silent lcd %s', dir))
						vim.cmd(selection.path)
					end
				},
			}
		},
	},
	extensions = {
		cmdline = {
			mappings = {
				run_selection = '<cr>',
				complete = '<cr>'
			},
		},
		docker = {
		      init_term = "tabnew",
		},
		project = {
			base_dirs = { '~/doc/code/' },
			hidden_files = true,
			theme = 'dropdown',
			order_by = 'asc',
			search_by = 'title',
			sync_with_nvim_tree = true,
			on_project_selected = function(prompt_bufnr)
			       project_actions.change_working_directory(prompt_bufnr, false)
			       --require("harpoon.ui").nav_file(1)
			end,
			display_type = 'full',

		},
		cder = {
			--dir_command = { 'fd', '--hidden', '--type=d', '.', os.getenv('HOME') },
			dir_command = { 'rg', '--hidden', '--files'},
			previewer_command =
				'exa '..
				'-lah '..
				'/root/ ' ..
				'--color=always '..
				--'-T ' ..
				--'--level=3 '..
				'--icons '..
				'--git-ignore '..
				'--long '..
				'--no-permissions '..
				'--no-user '..
				'--no-filesize '..
				'--git '..
				'--ignore-glob=.git',

			--previewer_command='ls -lah',
			--dir_command = { 'fd', '--hidden', '--type=d', '.', os.getenv('HOME') },
			--pager_command='bat --plain --paging=always --pager="less -RS"',
		},
	},
	vimgrep_arguments = vimgrep_arguments,
		vimgrep_arguments = {
			--'grep',
			--'--extended-regexp',
			--'--color=never',
			--'--with-filename',
			--'--line-number',
			--'-b',
			--'--ignore-case',
			--'--recursive',
			--'--no-messages',
			--'--exclude-dir=*cache*',
			--'--exclude-dir=*.git',
			--'--binary-files=without-match'
			--'rg',
			--'~/',
			--'--no-heading',
			--'--with-filename',
			--'--line-number',
			--'--column',
			--'--smart-case',
			--'--hidden',
		},
		file_ignore_patterns = { "node_modules", "build", "dist", "yarn_lock", "%.cache", "%.wine"  },
	--},
})

--vimgrep_arguments = {'echo', '"piranha"'}
--
--for _,i in ipairs( vimgrep_arguments ) do 
--	print( i )
--end
