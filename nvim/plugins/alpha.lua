local alpha = require( 'alpha' )
local dashboard = require( 'alpha.themes.dashboard' )

dashboard.section.header.val = {
	"                                                     ",
	" ████████╗ ██████╗ ██████╗  ██████╗ █████╗ ████████╗ ",
	" ╚══██╔══╝██╔═══██╗██╔══██╗██╔════╝██╔══██╗╚══██╔══╝ ",
	"    ██║   ██║   ██║██████╔╝██║     ███████║   ██║    ",
	"    ██║   ██║   ██║██╔═══╝ ██║     ██╔══██║   ██║    ",
	"    ██║   ╚██████╔╝██║     ╚██████╗██║  ██║   ██║    ",
	"    ╚═╝    ╚═════╝ ╚═╝      ╚═════╝╚═╝  ╚═╝   ╚═╝    ",
	"                                                     ",
}

dashboard.section.buttons.val = {
	dashboard.button( "e", "🗒️ > New file" , ":ene <BAR> startinsert <CR>"),
	dashboard.button( "f", "🔎 > Find file", ":cd $HOME/ | Telescope find_files<CR>"),
	dashboard.button( "r", "⏳ > Recent"   , ":Telescope oldfiles<CR>"),
	dashboard.button( "k", "📋 > Kanban"   , ":KanbanOpen ~/vimwiki/tasks.md<cr>"),
	dashboard.button( "s", "⚙️  > Settings" , ":e $MYVIMRC | :cd %:p:h | split . | wincmd k | pwd<CR>"),
	dashboard.button( "q", "⚜️  > Quit NVIM", ":qa<CR>"),
}

dashboard.section.footer.val = ''

alpha.setup(dashboard.opts)

vim.cmd([[
    autocmd FileType alpha setlocal nofoldenable
]])

vim.api.nvim_create_autocmd("VimEnter", {
  once = true,
  callback = function()
    math.randomseed(os.time())
    local fg_color = tostring(math.random(0, 12))
    local hi_setter = "hi AlphaHeader ctermfg="
    vim.cmd(hi_setter .. fg_color)
  end
})
