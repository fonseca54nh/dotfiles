Plugins = {
	{ 'tpope/vim-surround'            } ,
	{ 'scrooloose/nerdtree'           } ,
	{ 'godlygeek/tabular'             } ,
	{ 'neovim/nvim-lspconfig'         } ,
	{ 'ap/vim-css-color'              } ,
	{ 'majutsushi/tagbar'             } ,
	{ 'dylanaraps/wal.vim'            } ,
	{ 'turbio/bracey.vim'             } ,
	{ 'nvim-lua/plenary.nvim'         } ,
	{ 'junegunn/goyo.vim'             } ,
	{ 'edluffy/hologram.nvim'         } ,
	{ 'NeogitOrg/neogit'              } ,
	{ 'mattn/calendar-vim'            } ,
	{ 'zane-/cder.nvim'               } ,
	{ 'dhruvasagar/vim-table-mode'    } ,
	{ 'nvim-telescope/telescope.nvim' } ,
	{ 'ArbitRandomUser/latexrenderer' } ,
	{ 'kien/rainbow_parentheses.vim'  } ,
	{ '3rd/image.nvim'                } ,
	{ 'hrsh7th/cmp-nvim-lsp'          } ,
	{ 'hrsh7th/cmp-buffer'            } ,
	{ 'hrsh7th/cmp-path'              } ,
	{ 'hrsh7th/cmp-cmdline'           } ,
	{ 'hrsh7th/cmp-vsnip'             } ,
	{ 'hrsh7th/vim-vsnip'             } ,
	{ 'hrsh7th/nvim-cmp'              } ,
	{ 'ayu-theme/ayu-vim'             } ,
	{ 'arakkkkk/kanban.nvim'          } ,
	{ 'rafamadriz/friendly-snippets'  } ,
	{ 'mfussenegger/nvim-dap'         } ,
	{ 'theHamsta/nvim-dap-virtual-text' },
	{ 'kihachi2000/yash.nvim'         } ,
	{ 'sainnhe/everforest' 		  } ,
	{ 'sainnhe/sonokai' 		  } ,
	{ 'savq/melange-nvim' 		  } ,
	{ 'cocopon/iceberg.vim' 	  } ,
	{ 'junegunn/seoul256.vim'	  } ,
	{ 'fenetikm/falcon' 		  } ,
	{ 'projekt0n/caret.nvim' 	  } ,
	{ 'pineapplegiant/spaceduck'	  } ,
	{ 'w0ng/vim-hybrid' 		  } ,
	{ 'lpoto/telescope-docker.nvim'   } ,
	{ 'jonarrien/telescope-cmdline.nvim' } ,
	{ 'nvim-tree/nvim-web-devicons'   } ,
	{
	  'otavioschwanck/telescope-cmdline-word.nvim',
	  opts = { add_mappings = true, -- add <tab> mapping automatically
	    }
	},
	{ 'nvim-telescope/telescope-project.nvim' },
	{
	  'stevearc/overseer.nvim',
	  opts = {},
	},
	{
	  'zootedb0t/citruszest.nvim',
	  lazy = false,
	  priority = 1000,
	},
	{ 
	    'sebasruiz09/fizz.nvim',
	    lazy = false,
	    priority = 1000
	},
	{ 
	  'L3MON4D3/LuaSnip',
	  version = "v2.*",
	  build = "make install_jsregexp",
	  dependencies = { "rafamadriz/friendly-snippets" },
  	},
	{
	  "nvim-treesitter/nvim-treesitter",
	  version = false, -- last release is way too old and doesn't work on Windows
	  build = ":TSUpdate",
	  --event = { "LazyFile", "VeryLazy" },
	},
	{
	    'windwp/nvim-autopairs',
	    event = "InsertEnter",
	    config = true
	    -- use opts = {} for passing setup options
	    -- this is equalent to setup({}) function
	},
	{  
	    'nvim-telescope/telescope-fzf-native.nvim',
	    build = 'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build' 
        },
	{
		'chip/telescope-code-fence.nvim',
		--run = 'make install',
	},
	{
	    'nvim-lualine/lualine.nvim',
	    lazy = false,
	    dependencies = { 'nvim-tree/nvim-web-devicons' },
	    --options = { theme = 'gruvbox' }
	},
	{
	    "nvim-telescope/telescope-file-browser.nvim",
	    dependencies = { "nvim-telescope/telescope.nvim", "nvim-lua/plenary.nvim" }
	},
	--{
	--  'willothy/veil.nvim',
	--  lazy = true,
	--  event = 'VimEnter',
	--  config=true,
	--  dependencies = {
	--    -- All optional, only required for the default setup.
	--    -- If you customize your config, these aren't necessary.
	--    "nvim-telescope/telescope.nvim",
	--    "nvim-lua/plenary.nvim",
	--    "nvim-telescope/telescope-file-browser.nvim"
	--  },
	--},
	{
	    'goolord/alpha-nvim',
	    dependencies = { 'nvim-tree/nvim-web-devicons' },
	    config = function ()
		require'alpha'.setup(require'alpha.themes.dashboard'.config)
	    end
	},
	--{
	--  'MarcHamamji/runner.nvim',
	--  dependencies = {
	--    'nvim-telescope/telescope.nvim',
	--    dependencies = { 'nvim-lua/plenary.nvim' }
	--  }, 
	--  config = function()
	--    require('runner').setup()
	--  end
	--},

	{ 
		'vimwiki/vimwiki',
		init = function()
			vim.g.vimwiki_list = {
				{path='~/vimwiki', syntax='markdown', ext='.md', },
			} 
			vim.g.vimwiki_ext2syntax = { ['.md'] = 'markdown', ['.markdown'] = 'markdown', ['.mdown'] = 'markdown' }
      vim.g.vimwiki_use_mouse = 1
      vim.g.vimwiki_markdown_link_ext = 1
		end,

	} ,
}
