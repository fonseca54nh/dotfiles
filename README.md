# Topcat's dotfiles

![Desktop](doc/desktop.png)

## Setup


### Nvim

#### Kanban

Create a folder for your tasks inside vimwiki:

```
mkdir -p ~/vimwiki/tasks
```

#### Treesitter

Treesitter will complain about the parsers, so:

```
TSInstall markdown
TSInstall markdown_inline
```

#### Dependencies

##### Language servers( Optional )


Installing language servers are optional and should be a choice of the user. For this specific configuration I'm using these:

```
[lua-language-server](https://luals.github.io/wiki/build/)
```

If you don't install these nvim may cry a lot when editing a file from one of these languages! Don't worry, it it just missing these lsp.
