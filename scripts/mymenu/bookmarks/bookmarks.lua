local topics = {}


local file = io.open( 'bookmarks.md', 'r' )

if file then
	for line in file:lines() do
		local topic = line:match( '%[.*%]' )
		if topic then
			topic = topic:gsub( '%[ ', '' ):gsub( ' %]', '' )
			table.insert( topics, topic )
		end
	end
end

for _,i in ipairs( topics ) do
	print( i )
end
