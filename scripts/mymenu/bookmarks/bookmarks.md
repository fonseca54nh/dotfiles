# Topcat's Bookmarks

## estufaFixed

[ Github ](https://github.com/sghufsc)

[ Drive ](https://drive.google.com/drive/folders/1QDflqGVHQ-ajPAHmr8CVjw-jiKy30baf?usp=sharing)

[ Slack ](https://app.slack.com/client/T02B1R65KNC/C02ACCZ5S1Z?cdn_fallback=2)

[ Perguntas Fuzzy ]( https://docs.google.com/document/d/1MwmpgIueCAlp081Dk7ontpO4daZw6DKN/edit )

[ Esquemas Eletricos ](https://drive.google.com/drive/folders/1wbNgSDoUTKWYXMbLaIHK8jzqWVpx4Kgg)

[ Post form ](https://github.com/sghufsc/apian/blob/main/schemas/sensors.schema.json)

## estufaUtilities

[ Program to read davis vantage pro 2 ](http://www.joejaworski.com/weather/)

[ Update to use on raspberr pi ](https://github.com/waisbrot/vantage-pro-weather)

[ Davis 2 Manual ](https://manualzilla.com/doc/5792023/davis-vantage-pro2-manual)

[ Weather Link Manual ](https://cdn.shopify.com/s/files/1/0515/5992/3873/files/07395-210_gsg_06510_6555.pdf?v=1623784827)

[ Linux Serial-To-Usb connection ](https://www.fir3net.com/UNIX/Linux/how-do-i-connect-to-a-serial-device-using-a-usb-to-serial-convertor-in-linux.html)

[ This made chrony work as a server ](https://xitoring.com/kb/install-ntp-chrony-server-centos8/)

[ good chrony examples ](https://www.golinuxcloud.com/configure-chrony-ntp-server-client-force-sync/)

[ Chrony and soystemd ](https://prog.world/linux-time-synchronization-ntp-chrony-and-systemd-timesyncd/)

[ Gentoo wiki about chrony ](https://wiki.gentoo.org/wiki/Chrony)

[ Rasp and RTC ](https://pimylifeup.com/raspberry-pi-rtc/)

[ Rasp and RTC - Really good video ](https://www.youtube.com/watch?v=HP82VQxmfgE&list=LL&index=3&t=292s)

[ Tutorial cited on the video above ](https://thepihut.com/blogs/raspberry-pi-tutorials/17209332-adding-a-real-time-clock-to-your-raspberry-pi)

[ raspberry e senha padrao ](https://forums.raspberrypi.com/viewtopic.php?t=210106)

[ chrony and iptables ](https://www.server-world.info/en/note?os=CentOS_6&p=ntp&f=3)

## Pandoc

[ Lua filters for pandoc ](https://stackoverflow.com/questions/63525377/how-to-add-a-border-to-code-blocks-in-pandoc)

## OpenGL

[ How to create a lookAt function ](https://stackoverflow.com/questions/19740463/lookat-function-im-going-crazy#19740748)

[ How to create a projection matrix ](https://stackoverflow.com/questions/18404890/how-to-build-perspective-projection-matrix-no-api#18406650)

[ How to create a mvp matix ](https://stackoverflow.com/questions/21601692/setting-up-a-mvp-matrix-in-opengl)

[ Eh ](https://stackoverflow.com/questions/21601692/setting-up-a-mvp-matrix-in-opengl)

[ I was using this to create a terrain in ceng ](https://stackoverflow.com/questions/31696401/c-opengl-terrain-generation#31698270)

[ Mouse Picking ](https://antongerdelan.net/opengl/raycasting.html)

[ UI and shader ](https://www.reddit.com/r/opengl/comments/7fmune/ui_design_with_shaders/)

[ Fast blur ](https://fiveko.com/programming/fast-blur-filter-using-opengl-webgl/)

## C

[ Possible function overloading ](https://stackoverflow.com/questions/479207/how-to-achieve-function-overloading-in-c)

## To buy

[ Gaiola ](https://www.magazineluiza.com.br/gaiola-para-calopsita-azul-jel-plast/p/hh8he3kgeh/pe/otpe/)

## Grep

[ Use list as filter with grep ](https://askubuntu.com/questions/595426/use-a-list-of-words-to-grep-in-an-other-list)

## Sed

[ Find between two patterns ](https://www.shellhacks.com/sed-awk-print-lines-between-two-patterns/)

[ Use bash variable in sed command ](http://askubuntu.com/questions/76808/ddg#76842)

[ Exclude first and last line ](https://stackoverflow.com/questions/14913669/delete-first-and-last-line-or-record-from-file-using-sed)

[ Remove empty lines from a pipe ](https://stackoverflow.com/questions/40582635/removing-empty-lines-from-a-pipe-using-sed#40583346)

## Bash

[ For loop ](https://linuxize.com/post/bash-for-loop/)

[ Get things in parenthesis ](https://unix.stackexchange.com/questions/108250/print-the-string-between-two-parentheses)

## Conference

[ Terco PSE ]( https://meet.jit.si/testeTerco )                      

[ GOU ]( https://meet.google.com/onw-hxsm-jhc )                

[ Topicos II - Jitsi ]( https://meet.jit.si/aulaTopicos2 )                    

[ Tópicos II ]( https://meet.google.com/ykm-gwpn-jek )                

[ Tópicos II Git( hub ]https://github.com/fabiorochaufsc/TopicosI )          

[ Circuitos Digitais Github ](https://github.com/fabiorochaufsc/circuitosdigitais ) 

[ Embarcados Github ]( https://github.com/fabiorochaufsc/sistemasDigitalsEmbarcados )

[ Microprocessadores ]( https://meet.google.com/jua-vbov-eiw )                        

[ Sistemas Distribuidos ]( https://meet.google.com/stw-ahwj-ept )                

[ Redes ]( https://meet.google.com/evp-fibf-irz )                

[ Organizacao ]( https://meet.google.com/ipf-gcaq-awx )                

## Godot

[ Godot button press ](https://docs.godotengine.org/en/stable/classes/class_button.html)

[ Godot load scene ](https://godotengine.org/qa/24773/how-to-load-and-change-scenes)

## Neovim

[ Vim Graphical preview ](https://github.com/bytesnake/vim-graphical-preview)

[ Vim markdown fenced code ](https://github.com/tpope/vim-markdown)

[ Vim todo conceal ](https://gist.github.com/huytd/668fc018b019fbc49fa1c09101363397)

[ VIm todo conceal 2 ](https://www.reddit.com/r/vim/comments/h8pgor/til_conceal_in_vim/)

[ Vim org mode ](https://github.com/nvim-orgmode/orgmode)

[ Vim org mode 2 ](https://github.com/nvim-orgmode/orgmode/blob/master/syntax/org.vim)

[ Vim neorg ](https://github.com/nvim-neorg/neorg)

[ Vim neorg 2 ](https://user-images.githubusercontent.com/76052559/150838418-b443b92d-186a-45cb-ba84-06f03cdeea8a.png)

[ Vim conceal markdown links ](https://vi.stackexchange.com/questions/26825/conceal-markdown-links-and-extensions)

[ Cool Markdown Plugin ](https://github.com/SidOfc/mkdx)

[ Hologram - images ](https://github.com/edluffy/hologram.nvim)

## Unicode

[ Start symbols ](https://unicode-table.com/en/sets/star-symbols/)

[ Fisheye ](https://unicode-table.com/en/search/?q=Fisheye)

## Terminal

[ Lookatme markdown presentations ](https://github.com/d0c-s4vage/lookatme)

## Latex

[ Math symbols ](https://oeis.org/wiki/List_of_LaTeX_mathematical_symbols)

[ Generate a image sized pdf with latex ](https://tex.stackexchange.com/questions/134573/saving-tex-formulae-as-images)

## VHDL

[ repositorio ](https://github.com/cadu08/VHDL)

## STM32

[ libraries ](https://github.com/STMicroelectronics/STM32CubeF4)
