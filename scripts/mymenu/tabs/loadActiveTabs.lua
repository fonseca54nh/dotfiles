#!/bin/lua

local file = io.open( '/home/topcat/scripts/activeTabs.md', 'rb' )

local lines = {}
local temp  = {}
local header = ""

if file then
	for line in file:lines() do
		if line:find( '%#%s%w+' ) then
			lines[ header ] = temp
			temp = {}
			header = string.gsub( line, "%# ", "" )
		else
			if line ~= "" then table.insert( temp, line ) end
		end

	end
	lines[ header ] = temp
end

for index,i in pairs( lines )do
	print( index )
end

function Ret( a )
	local s = ""
	for index,i in pairs( a ) do
		s = s .. index .. "\n"
	end

	return ( s )
end

local a = Ret( lines )
local handle = io.popen( "echo \"" .. a .. "\" | dmenu -i -p \"Load Tabs:\" -l 40"  )

if handle then handle = handle:read('*all'):gsub( "\n", "" ) end


os.execute( 'brave-bin' )
for _,i in ipairs( lines[handle] ) do
	print( "opening " .. i .. " in browser")
	if i ~=nil then os.execute( "prime-run brave-bin " .. i ) end
end
