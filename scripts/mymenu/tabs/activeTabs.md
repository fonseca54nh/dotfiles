# Artigo Alisson

https://ieeexplore.ieee.org/abstract/document/7763386
https://ieeexplore.ieee.org/search/searchresult.jsp?newsearch=true&queryText=multi%20agents%20additive%20manufacturing
https://ieeexplore.ieee.org/document/7560316
https://ieeexplore.ieee.org/search/searchresult.jsp?newsearch=true&queryText=distributed%203d%20printing%20multi%20agents
https://ieeexplore.ieee.org/search/searchresult.jsp?newsearch=true&queryText=multi%20agents%20additive%20manufacturing
https://ieeexplore.ieee.org/document/7529573
https://ieeexplore.ieee.org/document/8791954
https://www.sciencedirect.com/search?qs=multi%20agents%20assembly%20line&years=2022%2C2023&publicationTitles=277340&offset=25
https://app.diagrams.net/#LUntitled%20Diagram
https://app.diagrams.net/#G1WvhlrXMH_XhA57ts-br54cTGvkyYw6Af

# Chess

https://lichess.org/study/18sdSEtd
https://www.chess.com/lessons/opening-principles/active-pieces
https://www.chess.com/lessons/advanced-endgames/common-rook-endgames

# Lua graph plot

https://www.amved.com/milindsweb/lua_plot.html
https://en.wikibooks.org/wiki/OpenGL_Programming/Scientific_OpenGL_Tutorial_01
https://asbradbury.org/projects/lua-imlib2/
https://asbradbury.org/projects/lua-imlib2/doc/
https://github.com/asb/lua-imlib2/blob/master/examples/wallpaper.lua

# Fish game art

https://duckduckgo.com/?q=american+fish+species&t=brave&iax=images&ia=images&iai=https%3A%2F%2Fimg1.etsystatic.com%2F000%2F0%2F5276420%2Fil_fullxfull.281222609.jpg
https://img1.etsystatic.com/000/0/5276420/il_fullxfull.281222609.jpg
https://i.pinimg.com/originals/e9/49/61/e94961d9f7be1b1ecbfc6018e860ddee.jpg

# Youtube

https://www.youtube.com/watch?v=sqmRYoNPbwI
https://www.youtube.com/watch?v=_MBlONltXyM
https://www.youtube.com/c/AquascapingCube

# Lua loadfile

https://stackoverflow.com/questions/29655452/load-bytecode-with-load-function
https://stackoverflow.com/questions/40767960/lua-loadfile-and-call-a-function-defined-in-the-file-by-name

# Lua glfw

https://solveforum.com/forums/threads/solved-glfw-resizing-causing-image-scaling.223717/

# Servidor

https://lucid.app/lucidspark/32153eeb-8df5-4632-9a0e-17fc063a7bbc/edit?invitationId=inv_89f27b59-71f3-4d0f-a884-e10a6918e7c4#

# Udemy courses

https://www.udemy.com/course/julialang/
https://www.udemy.com/course/tensorflow-hub-deep-learning-computer-vision-nlp/

# Gentoo

https://wiki.gentoo.org/wiki/Binary_package_guide

# Language learning

https://www.englishspeak.com/en/english-phrases?category_key=1
https://englishgrammarhere.com/vocabulary/500-most-common-english-words/
https://www.talkinfrench.com/french-past-tense/

# Language Tool

https://github.com/rhysd/vim-grammarous
https://languagetool.org/dev
https://github.com/languagetool-org/languagetool

# Pandoc

https://www.arthurkoziel.com/convert-md-to-html-pandoc/

# Requerimento geral

https://computacao.paginas.ufsc.br/files/2018/02/Requerimento-atividades-complementares-ENC-EDIT%C3%81VEL.pdf

# Blender

https://cdn1.epicgames.com/ue/product/Screenshot/05-1920x1080-15e7e197a30fe8ab1e38b32d58f1fb0a.jpg?resize=1&w=1600
https://cdn1.epicgames.com/ue/product/Screenshot/screenshot000_J-1920x1080-bb1838c13807f8cd55b4d1367860bb21.jpg?resize=1&w=1600
https://cdn.80.lv/api/upload/meta/16674/images/60f5bbf0b6e04/contain_1200x630.jpg
https://cdn1.epicgames.com/ue/product/Screenshot/064-1920x1080-2234046f2cbbe6a8ccffaf838405bdcf.jpg?resize=1&w=1600
https://www.youtube.com/watch?v=D7gXNmbqtwA
https://www.youtube.com/watch?v=esOpJTffPZ0

# Vim

https://duckduckgo.com/?q=vim+abbreviation+range&t=brave&ia=web
https://vi.stackexchange.com/questions/10797/change-the-filetype-based-on-tags-regions-in-the-file
https://stackoverflow.com/questions/61694504/how-to-make-coc-show-linting-popup-no-matter-where-the-cursor-is-on-the-line
https://vi.stackexchange.com/questions/31189/how-can-i-get-the-current-cursor-position-in-lua
https://stackoverflow.com/questions/33643249/vim-autocmd-bufnewfile-executed-multiple-times#33644413
https://github.com/martinduartemore/vim_agentspeak_jason

# Missa

https://docs.google.com/document/d/1jZMI_yjw_FTQUMFhvoD-L2V8Wown66X67XQH8BDuqJQ/edit
https://ocantonaliturgia.pt/sugestoes/92/15-de-Setembro-%7C-Mem%C3%B3ria-de-Nossa-Senhora-das-Dores
https://ocantonaliturgia.pt/
https://liturgia.pt/leccionarios/lec_dom_a.php
https://www.dropbox.com/s/trv78jtblx2hq75/Tons%20Salm%C3%B3dicos.pdf?dl=0
https://www.youtube.com/watch?v=yQddlSIddhU
https://cantabo-cs.com/wp-content/uploads/2021/06/Virgem-dolorosa-Sequ%C3%AAncia1.pdf
https://ocantonaliturgia.pt/obras/3476/Alegrai-vos-por-comungar-F-Silva
https://ocantonaliturgia.pt/obras/2661/Se-participardes-nos-sofrimentos-F-Santos

# 3D

https://www.3dgep.com/multi-textured-terrain-in-opengl/
https://ezinearticles.com/?OpenGL-Terrain-Generation---An-Introduction&id=231988
https://www.geeks3d.com/hacklab/20180514/demo-wireframe-shader-opengl-3-2-and-opengl-es-3-1/
https://blogs.igalia.com/itoral/2016/10/13/opengl-terrain-renderer-rendering-the-terrain-mesh/
https://www.learnopengles.com/tag/triangle-strips/
https://www.youtube.com/watch?v=V4UakVeat_4&t=284s
https://www.youtube.com/watch?v=01E0RGb2Wzo
https://stackoverflow.com/questions/1960560/how-does-3d-collision-object-detection-work#1960655
https://en.wikipedia.org/wiki/Gilbert%E2%80%93Johnson%E2%80%93Keerthi_distance_algorithm
https://duckduckgo.com/?q=opengl+draw+points&t=brave&ia=web&iax=qa
https://www.versluis.com/2017/06/how-to-generate-terrains-from-heigh-maps-in-blender/
https://adrianboeing.blogspot.com/2011/02/ripple-effect-in-webgl.html

# Shoes

https://produto.mercadolivre.com.br/MLB-2199265255-sapato-brogue-social-de-amarrar-solado-em-couro-duplo-657-_JM#position=48&search_layout=grid&type=item&tracking_id=fe0a7282-043c-4a4c-8391-c6490291054a
https://produto.mercadolivre.com.br/MLB-2211255734-sapato-masculino-democrata-sola-de-couro-social-cadarco-moda-_JM#position=13&search_layout=grid&type=item&tracking_id=fe0a7282-043c-4a4c-8391-c6490291054a
https://www.adolfoturrion.com.br/sapatos-classicos-sociais/sapato-social-derby-brogue-barceo-caramelo
https://www.bigioni.com.br/social-premium
https://produto.mercadolivre.com.br/MLB-1849555639-sapato-social-estilo-italiano-wholecut-sola-de-couro-duplo-_JM#reco_item_pos=4&reco_backend=machinalis-adv-hybrid-cruella&reco_backend_type=low_level&reco_client=vip-pads&reco_id=133d4fd0-e8b5-491b-bcb2-b4a01ceb6b91&is_advertising=true&ad_domain=VIPCORE_RECOMMENDED&ad_position=5&ad_click_id=NzBjNjQ2ZTgtZDIzNy00ZWQ3LTg3M2MtMWNlZDM4ZjRlMmM3
https://produto.mercadolivre.com.br/MLB-2727036946-sapato-social-solado-de-couro-_JM#position=39&search_layout=grid&type=item&tracking_id=28059a0d-0588-419f-8c73-e28f2f7e65c1
https://produto.mercadolivre.com.br/MLB-1959483091-sapato-estilo-italiano-bigioni-premium-100-couro-legitimo-_JM#position=24&search_layout=grid&type=item&tracking_id=28059a0d-0588-419f-8c73-e28f2f7e65c1
https://produto.mercadolivre.com.br/MLB-2615161611-sapato-masculino-democrata-confortavel-madison-amarrar-couro-_JM#position=21&search_layout=grid&type=item&tracking_id=2492945f-3557-4552-bc66-24685c1446d7
https://produto.mercadolivre.com.br/MLB-2800119363-sapato-derby-metropolitan-harry-democrata-316101-vero-2023-_JM#position=28&search_layout=grid&type=item&tracking_id=f3c71e49-657d-4d7d-8600-396ed6ea99b6
https://produto.mercadolivre.com.br/MLB-2603940398-sapato-derby-social-malbork-em-couro-marrom-cadarco-2008-_JM#position=54&search_layout=grid&type=pad&tracking_id=abee0367-409c-4c1c-beb5-c8346b7939a5#position=54&search_layout=grid&type=pad&tracking_id=abee0367-409c-4c1c-beb5-c8346b7939a5&is_advertising=true&ad_domain=VQCATCORE_LST&ad_position=54&ad_click_id=NjE5N2EzYzItMTFjYi00Njg0LWFkM2EtMjgyMDBjN2UzMDQz
https://produto.mercadolivre.com.br/MLB-1453119288-sapato-masculino-social-malbork-couro-cafe-sola-couro-54214-_JM#position=52&search_layout=grid&type=item&tracking_id=bae4391d-3a11-450f-bd8d-e97ccb7fca9d

# TCC

https://scikit-fuzzy.readthedocs.io/en/latest/auto_examples/plot_tipping_problem.html
https://duckduckgo.com/?q=scikit+learn+default+deffuzyfication+method&t=brave&ia=web
https://scikit-fuzzy.readthedocs.io/en/latest/_modules/skfuzzy/control/controlsystem.html#ControlSystemSimulation.compute
https://codecrucks.com/mamdani-fuzzy-inference-system-concept/

# Tasks

https://docs.gitlab.com/ee/ci/quick_start/
https://docs.gitlab.com/ee/ci/quick_start/#ensure-you-have-runners-available

# MPV scripts

https://github.com/jonniek/mpv-playlistmanager
https://github.com/jonniek/mpv-scripts
