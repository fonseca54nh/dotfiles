local muspath = '/home/topcat/mus/'
local files = io.popen( 'ls ' .. muspath )

local a = ""
if files then
	for line in files:lines() do
		a = a .. line .. '\n'
	end
end

local handle = io.popen( "echo \"" .. a .. "\" | dmenu -i -p \"Load Tabs:\" -l 40"  )

if handle then handle = handle:read( '*all' ) end

os.execute( 'mpv ' .. muspath .. handle )
