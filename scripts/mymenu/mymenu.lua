local options = { 'pdf', 'music', 'tabs', 'wallpaper', 'liturgy', 'bookmarks', 'tasks', 'colors', 'open'  }

local a = ""

for _,i in ipairs( options ) do
	a = a .. i .. '\n'
end

local option = io.popen( "echo \"" .. a .. "\" | dmenu -i -p \"MyMenu:\" -l 40"  )
if option then option = option:read('*all'):gsub( "\n", "" ) end

print( 'entire prase: ' .. option )

local app
local command

if option and option:find( '%s%w+' ) then
	app = option:match( '%w+' )
	command = option:match( '%s%w+' ):gsub( '%s', '' )
	os.execute( 'lua /home/topcat/scripts/mymenu/' .. app .. '/' .. app .. '.lua' .. ' ' .. command )
else
	os.execute( 'lua /home/topcat/scripts/mymenu/' .. option .. '/' .. option.. '.lua' )
end


