local schemes = { 'gruvbox', 'nord' }

local a = ""
for _,i in ipairs( schemes ) do
	a = a .. i .. '\n'
end

local handle = io.popen( "echo \"" .. a .. "\" | dmenu -i -p \"Select Colorscheme:\" -l 40 "  )
if handle then handle = handle:read('*all'):gsub( "\n", "" ) end

local file
if handle then file = io.open( '/home/topcat/scripts/mymenu/colors/' .. handle, "r" ) end

local colors = {}

if file ~=nil then
	for line in file:lines() do
		table.insert( colors, { string.match( line, "%w+" ) ,
					string.match( line, "%#%w+" ) } )
	end
end

a = ""
for _,i in ipairs( colors ) do
	a = a .. i[1] .. ',' .. i[2] .. '\n'
end

handle = io.popen( "echo \"" .. a .. "\" | dmenu -i -p \"Select Color:\" -l 40 "  )
if handle then handle = handle:read('*all'):gsub( "\n", "" ):gsub( '%w+%,', '' ) end

os.execute( "printf \"" .. handle .. "\"| xclip -i -sel clip" )
