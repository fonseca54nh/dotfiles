local files = {}
local folders = { 'wallpaper', 'wallpaper/gruvbox' }

local out = io.popen( 'find /home/topcat/pic/wallpaper -name \\*' )

if out then
	for line in out:lines() do
		table.insert( files, line )
	end
end

local a = ''
for _,i in ipairs( files ) do
	if i ~= '/home/topcat/pic/wallpaper' and
	   i ~= '/home/topcat/pic/wallpaper/gruvbox' then
		a = a .. i:gsub( '/home/topcat/pic/wallpaper/', '' ) .. '\n'
	end
end

local option = io.popen( "echo \"" .. a .. "\" | dmenu -i -p \"Select Folder:\" -l 40"  )
if option then option = option:read('*all'):gsub( "\n", "" ) end

os.execute( 'feh --bg-scale /home/topcat/pic/wallpaper/' .. option )
