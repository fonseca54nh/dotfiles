local app = {}

app.st    = { 'alt+3', 'st'          }
app.sound = { 'alt+8', 'pavucontrol' }
app.ard   = { 'alt+7', 'ardour6'     }
app.gimp  = { 'alt+5', 'gimp'        }
app.brave = { 'alt+2', 'brave-bin'   }
app.zat   = { 'alt+3', 'zathura'     }
app.ch    = { 'alt+9', 'st -e doas chroot ~/debian /bin/bash -c \"export TERM=linux;zsh\"' }

os.execute( 'xdotool key ' .. app[ arg[1] ][1] )
os.execute( app[ arg[1] ][2] )
