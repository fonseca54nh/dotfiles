local files = {}
local folders = { 'dow', 'doc', 'ufsc', 'books' }

local out

for _,i in ipairs( folders ) do
	out = io.popen( 'find /home/topcat/' .. i .. ' -name \\*.pdf' )

	if out then
		for line in out:lines() do
			table.insert( files, { i, line } )
		end
	end
end

local b = ""
for _,i in ipairs( folders ) do
	b = b .. i .. '\n'
end

local option = io.popen( "echo \"" .. b .. "\" | dmenu -i -p \"Select Folder:\" -l 40"  )
if option then option = option:read('*all'):gsub( "\n", "" ) end

local a = ""
for index,i in ipairs( files ) do
	if i[1] == option then
		a = a .. i[2]:gsub( '/home/topcat/' .. option .. '/', '' ) .. '\n'
	end
end

local handle = io.popen( "echo \"" .. a .. "\" | dmenu -i -p \"Open PDF:\" -l 40"  )
if handle then handle = handle:read('*all'):gsub( "\n", "" ) end
if handle ~= "" then os.execute( 'zathura /home/topcat/' .. option .. '/' .. handle .. ' & xdotool key alt+3' ) end
